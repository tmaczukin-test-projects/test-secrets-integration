resource "vault_mount" "simple-support-test-kv1" {
  type = "kv"
  path = "simple-support-test-kv1"
}

resource "vault_mount" "simple-support-test-kv2" {
  type = "kv-v2"
  path = "simple-support-test"
}

resource "vault_generic_secret" "example-kv2-shared" {
  path = "${vault_mount.simple-support-test-kv2.path}/shared"
  data_json = "{\"KEY_1\":\"VALUE_1\",\"KEY_2\":\"VALUE_2\"}"
}

resource "vault_generic_secret" "example-kv1" {
  path = "${vault_mount.simple-support-test-kv1.path}/test"
  data_json = "{\"KEY_3\":\"VALUE_3\",\"KEY_4\":\"VALUE_4\"}"
}
