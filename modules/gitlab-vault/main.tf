provider "vault" {
  address = var.vault_addr
  token = var.vault_token
}


locals {
  iea_meta_key_gitlab_instance = "{{identity.entity.aliases.${vault_jwt_auth_backend.gitlab-jwt.accessor}.metadata.gitlab_instance}}"
  iea_meta_key_project_id = "{{identity.entity.aliases.${vault_jwt_auth_backend.gitlab-jwt.accessor}.metadata.project_id}}"

  vault_jwt_default_role_name = "simple-support-test-role"
}

resource "vault_policy" "gitlab-simple-support-test" {
  name = "simple-support-test-${var.gitlab_domain}-${var.gitlab_project_id}"

  policy = <<EOF
path "${var.simple_support_test_kv1_path}/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}

path "${var.simple_support_test_kv2_path}/data/shared" {
    capabilities = ["read", "list"]
}

path "${var.simple_support_test_kv2_path}/metadata/instance/${local.iea_meta_key_gitlab_instance}/project/${local.iea_meta_key_project_id}/*" {
    capabilities = ["delete", "list"]
}

path "${var.simple_support_test_kv2_path}/data/instance/${local.iea_meta_key_gitlab_instance}/project/${local.iea_meta_key_project_id}/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}
EOF
}

resource "vault_jwt_auth_backend" "gitlab-jwt" {
  type = "jwt"
  path = "${var.gitlab_domain}/jwt"

  jwks_url = "${var.gitlab_schema}://${var.gitlab_domain}/oauth/discovery/keys"
  bound_issuer = var.gitlab_domain

  default_role = local.vault_jwt_default_role_name
}

resource "vault_jwt_auth_backend_role" "gitlab-jwt" {
  backend = vault_jwt_auth_backend.gitlab-jwt.path

  role_name = local.vault_jwt_default_role_name
  role_type = "jwt"

  token_explicit_max_ttl = var.token_explicit_max_ttl

  token_policies = [
    vault_policy.gitlab-simple-support-test.name
  ]

  user_claim = "user_email"

  bound_claims = {
    project_id = var.gitlab_project_id
  }

  claim_mappings = {
    iss = "gitlab_instance"
    project_id = "project_id"
  }
}
