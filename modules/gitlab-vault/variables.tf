variable "vault_addr" {
  description = "Address of the HashiCorp Vault server"
  type = string
}

variable "vault_token" {
  description = "Token to authenticate with HashiCorp Vault server"
  type = string
}

variable "gitlab_schema" {
  description = "Schema of GitLab instance URL"
  type = string
  default = "https"
}

variable "gitlab_domain" {
  description = "Domain of GitLab instance that will be connected"
  type = string
}

variable "gitlab_project_id" {
  description = "ID of the GitLab Project that will be connected"
  type = number
}

variable "token_explicit_max_ttl" {
  description = "The maximum TTL for the generated tokens"
  type = number
  default = 300
}

variable "simple_support_test_kv1_path" {
  description = "Path to the test KV-V1 secret engine"
  type = string
}

variable "simple_support_test_kv2_path" {
  description = "Path to the test KV-V2 secret engine"
  type = string
}
