# Test GitLab CI secrets support

This project is an example of GitLab CI's support for secrets, using HashiCorp Vault.

It's a test project for <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26565>.

## How to run this example

This example uses the [`CI_JOB_JWT` defined for each job](https://gitlab.com/gitlab-org/gitlab/-/issues/207125) to
interact with `vault` and resolve defined secrets.

The project contains also scripting that will prepare the required configuration on the Vault server.

The project **does not** describe in details how to prepare GitLab and Vault installation and basic setup. This
must be handled by the user. The description will only define what requirements must be met.

### Requirements

1. GitLab instance with the [Generate JWT for authentication](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28063),
   [Expose CI secrets in API response](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34898) and
   [Specify custom path for Vault auth method](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40366) changes.

   For my tests of this example, I was using my development environment prepared with
   [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit/). I've manually checked out the latest `master`
   branch and started GitLab from it.

   Make sure you've enabled required GitLab features before proceeding. For that enter the Rails console, and run:

   ```ruby
   ::Feature.enable(:ci_secrets_management)
   ::Feature.enable(:ci_secrets_syntax)
   ```

   To confirm that the feature flags are enabled, you can run the following commands:
   ```ruby
   ::Feature.enabled?(:ci_secrets_management)
   ::Feature.enabled?(:ci_secrets_syntax)
   ```

   which in both cases should return `true`.

   **Warning:** Secrets support requires the paid version of GitLab, so apart of GitLab you also need to have an active
   license.

   **TODO:** Which plan is the minimal one that supports this? Starter, Premium or Ultimate? Need to find this out and
   mention the right plan here.

1. An unsealed [HashiCorp Vault server](https://www.vaultproject.io/). For the purpose of this example you can use
   a server started in the development mode and use the initial `root` token. You should be logged into Vault
   when executing the commands from the following steps.

   **However be aware, that such setup is highly discourage for a non-development usage**. You should especially
   not use the server in development mode nor the `root` token for a production usage. In that case a properly
   deployed and secured server, as well as a limited admin account with only required permissions applied should
   be used.

   You need to know the Vault Server URL and the token with root permissions (we will be configuring Vault
   to support our test example).

1. [GitLab Runner](https://docs.gitlab.com/runner/) installed and registered within GitLab instance. The Runner should
   be configured to use the [Docker executor](https://docs.gitlab.com/runner/executors/docker.html).
   For the purpose of this example it's not important whether it's a project specific, group or an instance runner.
   **Just make sure that it's registered and available for your test project!**

   What is required, is that the Vault server is accessible from within the containers created by Runner to
   execute the jobs.

   It's important to use Runner in the version created in this MR:
   https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2374.

   **Notice:** After it will be merged and new version will be released the standard installation method can be used.
   But until then the only way to get the binary/package containing the required changes is to download them
   [from our official S3 bucket](https://gitlab-runner-downloads.s3.amazonaws.com/vault-support/add-vault-secret-resolver/index.html).

   To install Runner from the S3 bucket you can use following commands (assuming that you are using Linux on
   `amd64`/`x86_64` machine):

   ```shell
   # Install GitLab Runner
   # Assuming that you're using Linux on amd64/x86_64 machine. For Mac OS use the gitlab-runner-darwin-amd64 binary
   wget https://gitlab-runner-downloads.s3.amazonaws.com/vault-support/add-vault-secret-resolver/binaries/gitlab-runner-linux-amd64 -O /tmp/gitlab-runner
   chmod +x /tmp/gitlab-runner

   # Import Helper Image for Runner's revision
   # Assuming that you're using amd64/x86_64 machine for your Docker host (including Docker for Mac)
   wget https://gitlab-runner-downloads.s3.amazonaws.com/vault-support/add-vault-secret-resolver/helper-images/prebuilt-x86_64.tar.xz -O /tmp/prebuilt.tar.xz
   docker import /tmp/prebuilt.tar.xz gitlab/gitlab-runner-helper:x86_64-7d0a6890

   # Start Runner
   /tmp/gitlab-runner run --config path/to/your/config.toml
   ```

1. For applying Vault configuration we will use [HashiCorp Terraform](https://www.terraform.io/). A project that allows
   managing infrastructure in a declarative way.

   The configuration in this project assumes that Terraform in version at least `0.13` will be used!x

1. On my development machine I'm using containerized [Traefik](https://containo.us/traefik/) and a custom, locally
   resolvable domain for all of my applications. Therefore I'm able to use a nice domain names like
   `gitlab.maczukin.dev` or `vault.maczukin.dev` which are reachable also from all my local containers.

   Description of this setup is out of the scope of this example. You however must make sure, that all blocks can
   communicate with each other properly, before you will continue.

### Setup

#### Cloning the project

After all applications are installed and ready, we can proceed with configuring the Vault server for the purpose
of this example.

But first, let's clone the project:

```shell
$ git clone https://gitlab.com/tmaczukin-test-projects/test-secrets-integration.git
$ cd test-secrets-integration
```

#### Creating a new project in GitLab and defining few settings

Let's also crate a project at our test GitLab instance, where the code will be pushed next. **But please don't
do this push yet!**. For now just create the project and note the ID that was assigned (you can find it at the
project's main page as well as at the main settings page of the project).

Next, let's open the Settings > CI/CD page and define three variables: `VAULT_SERVER_URL`, `VAULT_AUTH_PATH`.
Defining `VAULT_SERVER_URL` is required to enable the secrets mechanism with our configuration.
`VAULT_AUTH_PATH` is required, because we will configure a custom path for authentication. `VAULT_AUTH_ROLE` is
left undefined, which will set Vault to use the default role defined for the used auth method.

Given that GitLab is accessible at `gitlab.example.com` and Vault is accessible at `https://vault.example.com`, the
variables should be defined as:

- `VAULT_SERVER_URL` set to `https://vault.example.com`
- `VAULT_AUTH_PATH` set to `gitlab.example.com/jwt`

#### Preparing Terraform configuration

Before we continue, you should also adjust the Terraform configuration that we will next apply.

First, let's create variables file that will contain details about your Vault server:

```shell
$ cat > test.tfvars <<EOF
vault_addr = "https://vault.maczukin.dev"
vault_token = "__REDACTED__"
EOF
```

The `vault_addr` variable contains the URL of your Vault server. The `vault_token` contains the value
of the token (please remember that it needs to have the `root` or similar permissions so that Terraform
will be able to configure your server).

Next let's update the `main.tf` file, so that it will describe your specific GitLab instance and project
pairs. The committed version of the file contains details specific for my personal case. In the
file you can find comments describing what and how should be changed.

Considering the simplest case where you have one GitLab instance (available at `https://gitlab.example.com`)
with one project (of ID `12345`) that you will use for running these tests, the `main.tf` file should look like:

```hcl
module "vault-gitlab_example_com" {
  source = "./modules/gitlab-vault"

  vault_addr = var.vault_addr
  vault_token = var.vault_token

  simple_support_test_kv1_path = vault_mount.simple-support-test-kv1.path
  simple_support_test_kv2_path = vault_mount.simple-support-test-kv2.path

  gitlab_domain = "gitlab.example.com"
  gitlab_project_id = 12345
}
```

#### Applying the configuration to Vault

Having all the files defined, let's apply Terraform configuration:

```shell
$ terraform apply -var-file test.tfvars
```

Terraform will:

1. Mount the secret engine `kv-v1` at the path `simple-support-test-kv1`.

1. Mount the secret engine `kv-v2` at the path `simple-support-test`.

1. Load example values to both secret engines.

    For `simple-support-test-kv1/test` secret it will be:

    - `KEY_3` key with value `VALUE_3`
    - `KEY_4` key with value `VALUE_4`

    For `simple-support-test/shared` secret it will be:

    - `KEY_1` key with value `VALUE_1`
    - `KEY_2` key with value `VALUE_2`

1. Enable JWT auth method at the path defined as `{GITLAB_DOMAIN}/jwt`.

    The configuration of the auth method is defined as:

    ```hcl
    jwks_url = "${GITLAB_URL}/oauth/discovery/keys"
    bound_issuer = "${GITLAB_HOSTNAME}"
    default_role = "simple-support-test-role"
    ```

    where:

    - `jwks_url` points GitLab's URL for OAuth keys discover; Vault will use the keys to validate the JWT,
    - `bound_issure` is set to GitLab's hostname, which must equals to `iss` claim from the JWT; otherwise
       JWT will be not authorized,
    - `default_role` is set to use the `simple-support-test-role` role. The role doesn't exist yet, but it's
       not a problem for Vault and we will fill this gap in a moment. With this default role definition every
       JWT login that doesn't specify any custom role, will use the `simple-support-test-role` one by default.

1. Define `simple-support-test-role` role assigned to the enabled JWT auth method:

    The role is defined as:

    ```json
    {
        "role_type": "jwt",
        "policies": ["simple-support-test-${GITLAB_HOSTNAME}-${GITLAB_PROJECT_ID}"],
        "token_explicit_max_ttl": 300,
        "bound_subject": "${GITLAB_PROJECT_ID}",
        "user_claim": "user_email",
        "claim_mappings": {
            "iss": "gitlab_instance",
            "project_id": "project_id"
        }
    }
    ```

    The `token_explicit_max_ttl` value set to `300 seconds` forces the maximum TTL of the token generated by
    logging with this JWT role. Even if other default configuration allows a longer TTL.

    The `bound_subject` uses the subject of JWT, which by GitLab is defined as job's project ID. Therefore such
    configuration will be usable only for this one project.

    The `user_claim` value defines which claim from JWT should be used to bind the entity and an alias for the
    identity bound to the generated token. Please note that the setup script doesn't create any identities nor
    aliases upfront, which means that one will be generated by Vault at the login time. In a production scenario
    you would probably prefer to use a normal user accounts available at Vault or to bind this identity with some
    "application" account defined explicitly for the CI job usage of this project. This is crucial for a proper
    auditing.

    `claim_mappings` maps the JWT claims (`iss`, `project_id`) to metadata fields that will be
    assigned to the attached entity alias. The metadata fields will be next used to fill the ACL policy template
    and give access to proper secrets.

1. Define `simple-support-test-${GITLAB_HOSTNAME}-${GITLAB_PROJECT_ID}` policy:

    The policy is defined as:

    ```hcl
    path "simple-support-test-kv1/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
    }

    path "simple-support-test/data/shared" {
        capabilities = ["read", "list"]
    }

    path "simple-support-test/metadata/instance/{{identity.entity.aliases.auth_jwt_XXXX.metadata.gitlab_instance}}/project/{{identity.entity.aliases.auth_jwt_XXXX.metadata.project_id}}/*" {
        capabilities = ["delete", "list"]
    }

    path "simple-support-test/data/instance/{{identity.entity.aliases.auth_jwt_XXXX.metadata.gitlab_instance}}/project/{{identity.entity.aliases.auth_jwt_XXXX.metadata.project_id}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
    }
    ```

    Please note, that the template variables are using the `auth_jwt_XXXX`, where `XXXX` will be a specific value,
    unique for each of the JWT auth roles defined for each of the module sections prepared in `main.tf` file. 

    The policy gives the logged in entity following permissions:

    - full CRUD and listing capabilities for the `simple-support-test-kv1/*` secrets.
    - read and list secrets at the `simple-support-test/data/shared` path,
    - list and delete metadata of `simple-support-test/instance/:gitlab_hostname/project/:project_id/*` secrets,
    - full CRUD and listing capabilities for the `simple-support-test/instance/:gitlab_hostname/project/*` secrets,

### Execution

**Notice:** for the documentation purpose I will be using the URL of my own local GitLab instance. For your execution
please adjust the calls properly.

Having the Vault configured and all building blocks ready and connected we can proceed with execution of the
example configuration. For that we will add a new remote to the cloned repository and we will push the code
to our test GitLab instance:

```shell
$ git remote add test ssh://git@gitlab.maczukin.dev:2222/root/test-ci-secrets.git
$ git push test master
```

And this is all!

When the push will be finished, GitLab should trigger a new pipeline.

The pipeline will contain one job - `test 1`. The job will read two secrets - one from the KVv2 and one from the KVv1
engine - and will assign them to configured variables. Next it will print the values of these variables to the output
and compare if they contain the expected values.

<details>
<summary>Example job output:</summary>

```shell
Running with gitlab-runner 13.4.0~beta.59.gc5dd8238 (c5dd8238)
  on docker-runner-manager-1 5RemTHxb
Resolving secrets
Resolving secret "TEST_SECRET_1"...
Using "vault" secret resolver...
Resolving secret "TEST_SECRET_2"...
Using "vault" secret resolver...
Preparing the "docker" executor
Using Docker executor with image alpine:latest ...
Authenticating with credentials from /home/tmaczukin/.docker/config.json
Pulling docker image alpine:latest ...
Using docker image sha256:a24bb4013296f61e89ba57005a7b3e52274d8edd3ae2077d04395f806b63d83e for alpine:latest with digest alpine@sha256:185518070891758909c9f839cf4ca393ee977ac378609f700f60a771a2dfe321 ...
Preparing environment
Running on runner-5remthxb-project-42-concurrent-0 via apollo.h.maczukin.pl...
Getting source from Git repository
Fetching changes with git depth set to 50...
Reinitialized existing Git repository in /builds/root/test-ci-secrets/.git/
Checking out 96ab2e9c as update-job...
Skipping Git submodules setup
Executing "step_script" stage of the job script
$ echo $TEST_SECRET_1
/builds/root/test-ci-secrets.tmp/TEST_SECRET_1
$ [ "$(cat ${TEST_SECRET_1})" == "VALUE_1" ]
$ echo $TEST_SECRET_2
/builds/root/test-ci-secrets.tmp/TEST_SECRET_2
$ [ "$(cat ${TEST_SECRET_2})" == "VALUE_3" ]
$ date
Tue Aug 25 12:13:05 UTC 2020
Job succeeded
```

</details>
If the pipeline succeeded then **congratulations!** You have your first _"Read GitLab CI secrets from Hashicorp Vault"_
execution completed!

## Author

2020, Tomasz Maczukin, GitLab Inc.

## License

MIT

