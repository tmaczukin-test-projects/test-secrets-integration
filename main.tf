/*

  This file defines access configuration to the vault server, to the example secrets, for different
  GitLab Project environments.

  The sections committed bellow are specific for my personal projects and will not work for you.

  Before applying Terraform configuration, please ensure that you've replaced the values
  of `gitlab_domain` and `gitlab_project_id` with your personal details. Adjusting the
  modules name (like the `vault-gitlab_com`) is not required, but will make your configuration
  more clear.

  Three definitions for three different GitLab servers is also specific for my test case. If you
  own or want to run the test against only one GitLab instance then please leave just one `module`
  section and remove the rest.

  Good luck with testing!

*/

module "vault-gitlab_com" {
  source = "./modules/gitlab-vault"

  vault_addr = var.vault_addr
  vault_token = var.vault_token

  simple_support_test_kv1_path = vault_mount.simple-support-test-kv1.path
  simple_support_test_kv2_path = vault_mount.simple-support-test-kv2.path

  // These two values are specific for my personal project. Please adjust for
  // your usage
  gitlab_domain = "gitlab.com"
  gitlab_project_id = 20712541
}

module "vault-staging_gitlab_com" {
  source = "./modules/gitlab-vault"

  vault_addr = var.vault_addr
  vault_token = var.vault_token

  simple_support_test_kv1_path = vault_mount.simple-support-test-kv1.path
  simple_support_test_kv2_path = vault_mount.simple-support-test-kv2.path

  // These two values are specific for my personal project. Please adjust for
  // your usage
  gitlab_domain = "staging.gitlab.com"
  gitlab_project_id = 4880956
}

module "vault-gitlab_maczukin_dev" {
  source = "./modules/gitlab-vault"

  vault_addr = var.vault_addr
  vault_token = var.vault_token

  simple_support_test_kv1_path = vault_mount.simple-support-test-kv1.path
  simple_support_test_kv2_path = vault_mount.simple-support-test-kv2.path

  // These two values are specific for my personal project. Please adjust for
  // your usage
  gitlab_domain = "gitlab.maczukin.dev"
  gitlab_project_id = 42
}
