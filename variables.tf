variable "vault_addr" {
  description = "Address of the HashiCorp Vault server"
  type = string
}

variable "vault_token" {
  description = "Token to authenticate with HashiCorp Vault server"
  type = string
}
